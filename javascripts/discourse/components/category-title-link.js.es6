import Component from "@ember/component";
export default Component.extend({
  tagName: "h2",
  classNames: ["bold"],
  // icon name defined here so it can be easily overriden in theme components
  lockIcon: "lock",
});
