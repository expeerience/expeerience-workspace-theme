import { alias, not } from "@ember/object/computed";
import Controller, { inject as controller } from "@ember/controller";
import DiscourseURL from "discourse/lib/url";
import Category from "discourse/models/category";
import discourseComputed, { observes } from "discourse-common/utils/decorators";
import { inject as service } from "@ember/service";

export default Controller.extend({ 
  discoveryTopics: controller("discovery/topics"), 
  navigationCategory: controller("navigation/category"),
  application: controller(),
  router: service(),

  loading: false,
  panels: [],
  selectedTab: null,

  category: alias("navigationCategory.category"),
  noSubcategories: alias("navigationCategory.noSubcategories"),

  loadedAllItems: not("discoveryTopics.model.canLoadMore"),

  @observes("loadedAllItems")
  _showFooter: function () {
    this.set("application.showFooter", this.loadedAllItems);
  },
  @discourseComputed("category")
  isCategory(category) {
    if(!category){
      return false;
    } 
    return true;
  },

  @discourseComputed("category.subcategories")
  isParent(subcategories) {
    return subcategories && subcategories.length > 0;
  },
  
  selectedTab() {
    return this.selectedTab;
  },

  showMoreUrl(period) {
    let url = "",
      category = this.category;

    if (category) {
      url = `/c/${Category.slugFor(category)}/${category.id}${
        this.noSubcategories ? "/none" : ""
      }/l`;
    }

    url += "/top/" + period;

    const queryParams = this.router.currentRoute.queryParams;
    if (Object.keys(queryParams).length) {
      url =
        `${url}?` +
        Object.keys(queryParams)
          .map((key) => `${key}=${queryParams[key]}`)
          .join("&");
    }

    return url;
  },

  actions: {
    selectedTab() {
      return this.selectedTab;
    },

    changePeriod(p) {
      DiscourseURL.routeTo(this.showMoreUrl(p));
    },
  },
});
